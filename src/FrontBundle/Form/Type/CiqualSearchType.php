<?php

namespace FrontBundle\Form\Type;

use FrontBundle\Form\Field\CiqualSearchField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CiqualSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search',
                CiqualSearchField::class,
                array(
                    'label' => 'Search',
                    'mapped' => false,
                    'required' => false,
                    'trigger_search_min_chars' => 2
                )
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ciqual_search_type';
    }
}
