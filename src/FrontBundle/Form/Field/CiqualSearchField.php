<?php

namespace FrontBundle\Form\Field;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CiqualSearchField extends AbstractType
{
    /**
     * @return string
     */
    public function getParent()
    {
        return TextType::class;
    }

    /**
     * @param OptionsResolver $resolver
     *
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'trigger_search_min_chars' => 1,
                'attr' => ['class' => 'ciqual_search_bar']
            ]
        );
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars['trigger_search_min_chars'] = $options['trigger_search_min_chars'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ciqual_search_bar';
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'ciqual_search_bar';
    }
}
