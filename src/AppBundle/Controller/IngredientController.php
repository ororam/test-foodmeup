<?php

namespace AppBundle\Controller;

use AppBundle\Repository\IngredientRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Route(service="app.controller.ingredient_controller")
 */
class IngredientController extends FOSRestController
{

    /** @var IngredientRepository */
    public $ingredientRepository;

    /**
     * @Rest\Get("/ingredients")
     * @ApiDoc()
     *
     * TODO: Pagination...
     *
     * @return \AppBundle\Entity\Ingredient[]|array|View
     */
    public function getIngredientsAction()
    {
        $ingredients = $this->ingredientRepository->findAll();
        if ($ingredients === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        return $ingredients;
    }

    /**
     * @Rest\Get("/ingredient/{id}")
     * @ApiDoc()
     *
     * @param $id
     * @return \AppBundle\Entity\Ingredient|View|null|object
     */
    public function getIngredient($id)
    {
        $ingredient = $this->ingredientRepository->find($id);
        if ($ingredient === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        return $ingredient;
    }

    /**
     * @Rest\Get("/ingredient/search/")
     *
     * @Rest\QueryParam(name="q", nullable=false)
     *
     * @ApiDoc()
     *
     * @param ParamFetcher $paramFetcher
     * @return \AppBundle\Entity\Ingredient[]|array|View
     */
    public function searchIngredient(ParamFetcher $paramFetcher)
    {
        $searchString = $paramFetcher->get('q');
        $ingredients = $this->ingredientRepository->searchByName($searchString);

        return $ingredients;
    }
}
