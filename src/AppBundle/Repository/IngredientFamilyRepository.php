<?php

namespace AppBundle\Repository;

use AppBundle\Entity\IngredientFamily;

/**
 * IngredientFamilyRepository
 *
 */
class IngredientFamilyRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param array $csvDataRow
     * @return \AppBundle\Entity\IngredientFamily
     */
    public function createIngredientFamilyFromCsvArray($csvDataRow)
    {
        $ingredientFamily = new IngredientFamily();

        return $this->updateIngredientFamilyFromCsvArray($ingredientFamily, $csvDataRow);
    }

    /**
     * @param IngredientFamily $ingredientFamily
     * @param $csvDataRow
     * @return IngredientFamily
     */
    public function updateIngredientFamilyFromCsvArray(IngredientFamily $ingredientFamily, $csvDataRow)
    {
        $frenchName = mb_convert_encoding($csvDataRow['ORIGGPFR'], "UTF-8", "ISO-8859-15");
        $ingredientFamily->translate('en')->setName($frenchName);
        $ingredientFamily->translate('fr')->setName($frenchName);
        $ingredientFamily->setORIGGPCD($csvDataRow['ORIGGPCD']);

        $ingredientFamily->mergeNewTranslations();

        return $ingredientFamily;
    }
}
