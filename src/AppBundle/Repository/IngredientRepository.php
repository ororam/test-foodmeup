<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Ingredient;
use AppBundle\Entity\IngredientFamily;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\QueryBuilder;

/**
 * IngredientRepository
 *
 */
class IngredientRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param array $csvDataRow
     * @param IngredientFamily $ingredientFamily
     * @return Ingredient
     */
    public function createIngredientFromCsvArray($csvDataRow, IngredientFamily $ingredientFamily)
    {
        $ingredient = new Ingredient();
        return $this->updateIngredientFromCsvArray($ingredient, $csvDataRow, $ingredientFamily);
    }

    /**
     * @param Ingredient $ingredient
     * @param array $csvDataRow
     * @param IngredientFamily $ingredientFamily
     * @return Ingredient
     */
    public function updateIngredientFromCsvArray(Ingredient $ingredient, $csvDataRow, IngredientFamily $ingredientFamily)
    {
        $frenchName = mb_convert_encoding($csvDataRow['ORIGFDNM'], "UTF-8", "ISO-8859-15");
        $ingredient->translate('en')->setName($frenchName);
        $ingredient->translate('fr')->setName($frenchName);
        $ingredient->setORIGFDCD($csvDataRow['ORIGFDCD']);
        $ingredient->setIngredientFamily($ingredientFamily);

        $ingredientDataSkipCsvArrayKeys = ['ORIGGPCD', 'ORIGGPFR', 'ORIGFDCD', 'ORIGFDNM'];

        $ingredientDataArray = array();
        foreach ($csvDataRow as $key => $value) {
            if (!in_array($key, $ingredientDataSkipCsvArrayKeys)) {

                $key = trim(mb_convert_encoding($key, "UTF-8", "ISO-8859-15"));
                $value = trim(mb_convert_encoding($value, "UTF-8", "ISO-8859-15"));

                /** Skip keys containing unexpected chars in the CSV ... */
                if (strpos($key, ',') || strpos($key, ':')) {
                    continue;
                }

                /** Fix empty values ... */
                if (strlen($value) == 0 || strcmp($value, '-') == 0) {
                    $value = '0';
                }

                $ingredientDataArray[$key] = $value;
            }
        }

        $ingredient->setData($ingredientDataArray);
        $ingredient->mergeNewTranslations();

        return $ingredient;
    }

    /**
     * @param $string
     * @return array
     */
    public function searchByName($string)
    {
        $queryBuilder = $this->createQueryBuilder('ingredient');
        $queryBuilder
            ->select('ingredient.id')
            ->addSelect('ingredientTranslation.name AS name')
            ->addSelect('ingredientTranslationFamily.name AS family')
            ->join('ingredient.translations', 'ingredientTranslation')
            ->join('ingredient.ingredientFamily', 'ingredientFamily')
            ->join('ingredientFamily.translations', 'ingredientTranslationFamily')
            ->where(
                $queryBuilder->expr()->like('ingredientTranslation.name', ':search')
            )
            ->setParameter('search', '%'.$string.'%')
            ->distinct('id');

        return $queryBuilder->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
