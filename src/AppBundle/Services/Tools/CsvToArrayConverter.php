<?php

namespace AppBundle\Services\Tools;

class CsvToArrayConverter
{
    /**
     * @param $filename
     * @param string $delimiter
     * @return array|bool
     */
    public function convert($filename, $delimiter = ';')
    {
        if (!is_readable($filename)
            || ($handle = fopen($filename, 'r')) === false
        ) {
            return false;
        }

        $header = null;
        $data = array();

        while (($row = fgetcsv($handle, 50000, $delimiter)) !== false) {
            foreach($row as $key => $value) {
                $row[$key] = $value;
            }

            if ($header === null) {
                $header = $row;
            } else {
                $data[] = array_combine($header, $row);
            }
        }
        fclose($handle);

        return $data;
    }
}
