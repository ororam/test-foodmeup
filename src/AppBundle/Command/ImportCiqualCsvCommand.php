<?php

namespace AppBundle\Command;

use AppBundle\Repository\IngredientFamilyRepository;
use AppBundle\Repository\IngredientRepository;
use AppBundle\Services\Tools\CsvToArrayConverter;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCiqualCsvCommand extends Command
{
    use ProgressBarTrait;

    /** @var CsvToArrayConverter */
    public $csvToArrayConverter;

    /** @var IngredientFamilyRepository */
    public $ingredientFamilyRepository;

    /** @var IngredientRepository */
    public $ingredientRepository;

    /** @var EntityManager */
    public $entityManager;

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('import:ciqual:csv')
            ->setDescription('Import the CIQUAL CSV to the database')
            ->setHelp('You must specify a path using the --path option.')
            ->addOption(
                'path',
                null,
                InputOption::VALUE_REQUIRED,
                'Specify a path of your CSV file'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getOption('path');

        if (!$path || !is_readable($path)) {
            throw new \InvalidArgumentException('Cannot read the CSV file. You must specify a valid --path option.');
        }

        $csvData = $this->csvToArrayConverter->convert($path);
        $csvRowAmount = count($csvData);
        $output->writeln(sprintf("<info>%d rows found in the CSV file, importing...</info>", $csvRowAmount));
        $progressBar = $this->createProgressBar($output, $csvRowAmount);
        $ingredientFamilyRepository = $this->ingredientFamilyRepository;
        $ingredientRepository = $this->ingredientRepository;

        foreach ($csvData as $csvDataRow) {

            /** Add or Update an "IngredientFamily" row */
            $ingredientFamily = $ingredientFamilyRepository->findOneByORIGGPCD($csvDataRow['ORIGGPCD']);
            if (!$ingredientFamily) {
                $ingredientFamily = $ingredientFamilyRepository->createIngredientFamilyFromCsvArray($csvDataRow);
            } else {
                $ingredientFamily = $ingredientFamilyRepository->updateIngredientFamilyFromCsvArray($ingredientFamily, $csvDataRow);
            }
            $this->entityManager->persist($ingredientFamily);

            /** Add or Update an "Ingredient" row */
            $ingredient = $ingredientRepository->findOneByORIGFDCD($csvDataRow['ORIGFDCD']);
            if (!$ingredient) {
                $ingredient = $ingredientRepository->createIngredientFromCsvArray($csvDataRow, $ingredientFamily);
            } else {
                $ingredient = $ingredientRepository->updateIngredientFromCsvArray($ingredient, $csvDataRow, $ingredientFamily);
            }
            $this->entityManager->persist($ingredient);

            /* Flush every 30 rows for better performances */
            if ($progressBar->getProgress() % 30 === 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }

            $progressBar->advance();
        }

        $this->entityManager->flush();
        $progressBar->finish();
        $output->writeln(PHP_EOL . '<info>Done!</info>');
    }
}