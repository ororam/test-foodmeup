<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

trait ProgressBarTrait
{
    /**
     * @param OutputInterface $output
     * @param $total
     * @return ProgressBar
     */
    private function createProgressBar(OutputInterface $output, $total)
    {
        $progress = new ProgressBar($output, $total);
        $progress->setFormatDefinition(
            'custom',
            ' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s% %memory:6s% -- %message%'
        );
        $progress->setFormat('custom');
        $progress->setMessage('Start');

        $progress->start();

        return $progress;
    }
}
