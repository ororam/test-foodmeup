<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * IngredientFamily
 *
 * @ORM\Table(name="ingredient_family")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IngredientFamilyRepository")
 */
class IngredientFamily
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * This function makes it possible to call translated properties without having to know if the enitity is translated.
     *
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get'. ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $args);
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ORIGGPCD", type="string", length=4)
     */
    private $oRIGGPCD;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(mappedBy="ingredientFamily", targetEntity="AppBundle\Entity\Ingredient", cascade={"persist", "detach"})
     */
    private $ingredients;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set oRIGGPCD
     *
     * @param string $oRIGGPCD
     *
     * @return IngredientFamily
     */
    public function setORIGGPCD($oRIGGPCD)
    {
        $this->oRIGGPCD = $oRIGGPCD;

        return $this;
    }

    /**
     * Get oRIGGPCD
     *
     * @return string
     */
    public function getORIGGPCD()
    {
        return $this->oRIGGPCD;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ingredients = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ingredient
     *
     * @param \AppBundle\Entity\Ingredient $ingredient
     *
     * @return IngredientFamily
     */
    public function addIngredient(\AppBundle\Entity\Ingredient $ingredient)
    {
        $this->ingredients[] = $ingredient;

        return $this;
    }

    /**
     * Remove ingredient
     *
     * @param \AppBundle\Entity\Ingredient $ingredient
     */
    public function removeIngredient(\AppBundle\Entity\Ingredient $ingredient)
    {
        $this->ingredients->removeElement($ingredient);
    }

    /**
     * Get ingredients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }
}
