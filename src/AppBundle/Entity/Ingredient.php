<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Ingredient
 *
 * @ORM\Table(name="ingredient")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IngredientRepository")
 */
class Ingredient
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * This function makes it possible to call translated properties without having to know if the enitity is translated.
     *
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get'. ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $args);
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ORIGFDCD", type="string", length=45)
     */
    private $oRIGFDCD;

    /**
     * @var array
     *
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    private $data;

    /**
     * @var IngredientFamily
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\IngredientFamily", inversedBy="ingredients", cascade={"persist"})
     */
    private $ingredientFamily;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set oRIGFDCD
     *
     * @param string $oRIGFDCD
     *
     * @return Ingredient
     */
    public function setORIGFDCD($oRIGFDCD)
    {
        $this->oRIGFDCD = $oRIGFDCD;

        return $this;
    }

    /**
     * Get oRIGFDCD
     *
     * @return string
     */
    public function getORIGFDCD()
    {
        return $this->oRIGFDCD;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return Ingredient
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set ingredientFamily
     *
     * @param \AppBundle\Entity\IngredientFamily $ingredientFamily
     *
     * @return Ingredient
     */
    public function setIngredientFamily(\AppBundle\Entity\IngredientFamily $ingredientFamily = null)
    {
        $this->ingredientFamily = $ingredientFamily;

        return $this;
    }

    /**
     * Get ingredientFamily
     *
     * @return \AppBundle\Entity\IngredientFamily
     */
    public function getIngredientFamily()
    {
        return $this->ingredientFamily;
    }
}
